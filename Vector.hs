-- Copyright (C) 2013, Cameron White
-- All rights reserved.
-- 
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions
-- are met:
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright
--    notice, this list of conditions and the following disclaimer in the
--    documentation and/or other materials provided with the distribution.
-- 3. Neither the name of the project nor the names of its contributors
--    may be used to endorse or promote products derived from this software
--    without specific prior written permission.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
-- ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
-- FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
-- DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
-- LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
-- OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
-- SUCH DAMAGE.
module Vector where

data Vector = Vector [Double]
	deriving(Eq)

instance Show Vector where
	show = showVector

instance Num Vector where
	(+) = addVector
	(-) = subVector
	(*) = crossVector 
	negate = negateVector
	fromInteger = fromIntegerVector

showVector :: Vector -> String
showVector (Vector us) = show us

negateVector :: Vector -> Vector
negateVector u = scaleVector (-1) u

addVector :: Vector -> Vector -> Vector
addVector (Vector us) (Vector vs) = Vector (zipWith (+) us vs)

subVector :: Vector -> Vector -> Vector
subVector (Vector us) (Vector vs) = Vector (zipWith (-) us vs)

scaleVector :: Double -> Vector -> Vector
scaleVector x (Vector us) = Vector (map (*x) us)

fromIntegerVector :: Integer -> Vector
fromIntegerVector x = Vector [y] 
	where y = fromIntegral x :: Double

crossVector :: Vector -> Vector -> Vector
crossVector (Vector us) (Vector vs) = Vector (zipWith (*) us vs)

lengthVector :: Vector -> Int
lengthVector (Vector us) = length us

dot :: Vector => Vector -> Double
dot (Vector us) (Vector vs) = sum (zipWith (*) us vs)

magnitude :: Vector -> Double
magnitude u = dot u u

vget :: Vector -> Int -> Double
vget (Vector us) i = us !! i

removeElement :: Vector -> Int -> Vector
removeElement (Vector us) i = Vector (remove us i)

remove :: [a] -> Int -> [a]
remove xs i = (take i xs) ++ (drop (i+1) xs)



