module Logic where

implies :: Bool -> Bool -> Bool
implies True False = False
implies _    _     = True

iff :: Bool -> Bool -> Bool
iff True  True  = True
iff False False = True
iff _     _     = False

and :: Bool -> Bool -> Bool
and True True = True
and _    _    = False

or :: Bool -> Bool -> Bool
or False False = False
or _     _     = True

xor :: Bool -> Bool -> Bool
xor False False = False
xor True  True  = False
xor _     _     = True

not :: Bool -> Bool
not True  = False
not False = True

nand :: Bool -> Bool -> Bool
nand x y = not (x `and` y)

nor :: Bool -> Bool -> Bool
nor x y = not (x `or` y)
