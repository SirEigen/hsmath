-- Copyright (C) 2013, Cameron White
-- All rights reserved.
-- 
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions
-- are met:
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright
--    notice, this list of conditions and the following disclaimer in the
--    documentation and/or other materials provided with the distribution.
-- 3. Neither the name of the project nor the names of its contributors
--    may be used to endorse or promote products derived from this software
--    without specific prior written permission.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
-- ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
-- FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
-- DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
-- LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
-- OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
-- SUCH DAMAGE.
module Matrix where

import Vector

data Matrix = Matrix [Vector]
	deriving(Eq)

instance Show Matrix where
	show = showMatrix

instance Num Matrix where
	(+) = addMatrix
	(-) = subMatrix
	(*) = mmmul

showMatrix :: Matrix -> String
showMatrix (Matrix []) = ""
showMatrix (Matrix (v:vs)) | vs /= [] = vectorString ++ "\n" ++ matrixString
						   | otherwise = vectorString
						   		where vectorString = show v;
						   		      matrixString = showMatrix (Matrix vs)

addMatrix :: Matrix -> Matrix -> Matrix
addMatrix (Matrix mss) (Matrix nss) = Matrix (zipWith (+) mss nss)

subMatrix :: Matrix -> Matrix -> Matrix
subMatrix (Matrix mss) (Matrix nss) = Matrix (zipWith (-) mss nss)

scaleMatrix :: Double -> Matrix -> Matrix
scaleMatrix x (Matrix mss) = Matrix (map (scaleVector x) mss)

removeVector :: Matrix -> Int -> Matrix
removeVector (Matrix mss) i = Matrix ((take i mss) ++ (drop (i+1) mss))

mget :: Matrix -> Int -> Int -> Double
mget (Matrix mss) i j = vget (mss !! i) j

numberOfRows :: Matrix -> Int
numberOfRows (Matrix []) = 0
numberOfRows (Matrix mss) = length mss

numberOfColumns :: Matrix -> Int
numberOfColumns (Matrix []) = 0
numberOfColumns (Matrix mss) = lengthVector (mss !! 0)

transposeMatrix :: Matrix -> Matrix
transposeMatrix (Matrix mss) = 
	Matrix [ Vector (map (\v -> vget v i) mss) | i <- [0..length mss -1]]

mvmul :: Matrix -> Vector -> Vector
mvmul (Matrix mss) vs = Vector (map (\ms -> dot ms vs) mss)

mmmul :: Matrix -> Matrix -> Matrix
mmmul m (Matrix nss) = Matrix (map (mvmul (transposeMatrix m)) nss)

diagonal :: Matrix -> Vector
diagonal (Matrix mss) = Vector (zipWith (vget) mss [0..length mss - 1])

determinate :: Matrix -> Double
determinate (Matrix []) = 0
determinate mss = if (numberOfRows mss == 2) && (numberOfColumns mss == 2) 
                then (a * d) - (b * c)
                else sum (map determinate (submatrices mss))
                     where a = mget mss 0 0;
                           b = mget mss 0 1;
                           c = mget mss 1 0;
                           d = mget mss 1 1

submatrices :: Matrix -> [Matrix]
submatrices (Matrix mss) = 
	[ Matrix (map (\v -> removeElement v 0) (remove mss i)) | 
    	i <- [0..length mss - 1] ]

{-
ref :: Matrix -> Matrix
ref (Matrix (ms:[])) = [ms]
ref (Matrix (ms:mss)) =  ms : zipWith (:) (map head nss) (ref (map tail nss))
    where nss = map (elementryRowOpperation ms) mss
-}

elementryRowOpperation :: Vector -> Vector -> Vector
elementryRowOpperation us vs = vs - (scaleVector (v/u) us)
    where u = vget us 0;
          v = vget vs 0

